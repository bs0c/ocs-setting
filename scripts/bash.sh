#!/usr/bin/env bash

main()
{
    local tool=$1
    local conf_dir="$CONF_DIR/$tool/bashrc.d/*.sh"
    case $MODE in
        session)
            local dst=$BASH_INIT_FILE
            for conf_file in $conf_dir; do
                if grep -q "global settings" "$conf_file"; then
                    cat "$conf_file" >> "$dst"
                fi
            done
            ;;
        user)
            local dst="$XDG_CONFIG_HOME"
            cp --recursive "$CONF_DIR/$tool" "$dst/$tool"
            ;;
        system)
            local dst="/etc/profile.d"
            for file in $conf_dir; do
                if grep -q "global settings" "$file"; then
                    add_tag $file
                    cp "$file" "$dst"
                fi
            done
            ;;
    esac
}

main $@
