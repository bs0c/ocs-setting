#!/usr/bin/env bash

main()
{
    local tool=$1
    case $MODE in
        session)
            awk ' BEGIN { FS="###"; RS="EOF"}
                {
                    for (s = 2; s < NF; s+=2) {
                        if ( match($s, "less") ) {
                            print "###", $s, "###", $(s+1)
                            break
                        }
                    }
                }' < "$BASH_PROFILE" >> $BASH_INIT_FILE
            ;;
        user)
            local dst="$XDG_CONFIG_HOME"
            cp --recursive "$CONF_DIR/$tool" "$dst/$tool"
            ;;
        system)
            local dst="/etc"
            local conf_file="/etc/profile.d/less.sh"
            add_tag $CONF_DIR/$tool
            cp --recursive "$CONF_DIR/$tool" "$dst"
            awk ' BEGIN { FS="###"; RS="EOF"}
                {
                    for (s = 2; s < NF; s+=2) {
                        if ( match($s, "less") ) {
                            print "###", $s, "###", $(s+1)
                            break
                        }
                    }
                }' < "$BASH_PROFILE" | envsubst '$XDG_CONFIG_HOME' > $conf_file
            add_tag $conf_file
            ;;
    esac
}

main $@
