#!/usr/bin/env bash

main()
{
    tool=$1
    case $MODE in
        session)
            cat <<-EOF >> $BASH_INIT_FILE
				alias tmux="tmux -f $XDG_CONFIG_HOME/$tool/tmux.conf"
			EOF
            ;;
        user)
            local dst="$XDG_CONFIG_HOME"
            cp --recursive "$CONF_DIR/$tool" "$dst/$tool"
            ;;
        system)
            local dst="/etc"
            add_tag $CONF_DIR/$tool/${tool}.conf
            cp "$CONF_DIR/$tool/${tool}.conf" "$dst"
            ;;
    esac
}

main $@
