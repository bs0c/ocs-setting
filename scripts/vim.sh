#!/usr/bin/env bash

main()
{
    local tool=$1
    local src="$CONF_DIR/$1"
    case $MODE in
        session)
            cat <<-EOF >> $BASH_INIT_FILE
				alias vi="vim -S $src/vimrc"
				alias vim="vim -S $src/vimrc"
			EOF
            ;;
        user)
            local dst="$XDG_CONFIG_HOME"
            cp --recursive "$src" "$dst"
            ln --symbolic --force "$dst"/$tool "$HOME"/.$tool
            msg="${RED}run as root: \n${NOFORMAT}  find /usr/share/vim -type d -name 'vim[89]*' \ \n\t-exec cp $HOME/.config/vim/userrc/key_russian-colemake.vim {}/keymap/russian-colemak.vim \;"
            ;;
        system)
            local dst="/etc/$tool"
            local dst_local=$dst/userrc
            local listrc="$(find /etc -name 'vi*rc')"
            local localrc="$dst/vimrc.local"
            local TAG="\"$TAG"

            __mkdir $dst
            __mkdir $dst_local

            for file in $listrc; do
                if ! grep -q "source $localrc" $file; then 
                    cat >> $file <<-EOF 
						
						"###   ocs-setting   ###
						source $localrc
					EOF
                fi
            done

            cat "$src/vimrc" | envsubst '$XDG_CONFIG_HOME' > $localrc
            add_tag $localrc
            for file in $src/userrc/*; do
                if grep -q 'global setting' "$file"; then
                    add_tag $file
                    cp --force --recursive $file $dst_local
                fi
            done
            find /usr/share/vim -type d -name 'vim[89]*' \
                -exec cp $CONF_DIR/$tool/userrc/key_russian-colemak.vim {}/keymap/russian-colemak.vim \;
            ;;
    esac
}
 
main $@
