#!/usr/bin/env bash

echo 'cat /home/rat/ocs-setting/README' > bashrc

cat > Dockerfile <<EOF 
FROM almalinux:latest
RUN dnf upgrade -y && \
	dnf install -y gettext vim git tmux	diffutils&& \
	useradd -m rat && \
	mkdir /home/rat/ocs-setting
COPY bashrc /root/.bashrc
COPY bashrc /home/rat/.bashrc
# USER rat
ENV PS1 '\[\033[1;34m\]┌─[\[\033[1;33m\]\u@\H\[\033[1;34m\]]─[\[\033[1;33m\]\w\[\033[1;34m\]]\[\033[0m\]\n\[\033[1;34m\]└─$\[\033[0m\] '
WORKDIR /home/rat/ocs-setting
EOF

docker build -t ocs-setting .
docker run --rm -it -v $HOME/develop/ocs-setting:/home/rat/ocs-setting ocs-setting

rm bashrc Dockerfile
